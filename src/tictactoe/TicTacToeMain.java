package tictactoe;

/**
 * source: https://www.ntu.edu.sg/home/ehchua/programming/java/JavaGame_TicTacToe.html
 */
public class TicTacToeMain {
    public static void main(String[] args) {
        TicTacToeManager manager = new TicTacToeManager();
        manager.start();
    }
}
