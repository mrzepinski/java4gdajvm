package tictactoe.model;

public class Board {
    private static final int ROWS = 3;
    private static final int COLS = 3;

    private Cell[][] cells;
    private int currentRow;
    private int currentCol;

    public Board() {
        cells = new Cell[ROWS][COLS];
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                cells[row][col] = new Cell(row, col);
            }
        }
    }

    public static int getRows() {
        return ROWS;
    }

    public static int getCols() {
        return COLS;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public int getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }

    public int getCurrentCol() {
        return currentCol;
    }

    public void setCurrentCol(int currentCol) {
        this.currentCol = currentCol;
    }

    public void init() {
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                cells[row][col].clear();
            }
        }
    }

    public boolean isDraw() {
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                if (cells[row][col].getContent() == Seed.EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean hasWon(Seed theSeed) {
        return (cells[currentRow][0].getContent() == theSeed
            && cells[currentRow][1].getContent() == theSeed
            && cells[currentRow][2].getContent() == theSeed
            || cells[0][currentCol].getContent() == theSeed
            && cells[1][currentCol].getContent() == theSeed
            && cells[2][currentCol].getContent() == theSeed
            || currentRow == currentCol
            && cells[0][0].getContent() == theSeed
            && cells[1][1].getContent() == theSeed
            && cells[2][2].getContent() == theSeed
            || currentRow + currentCol == 2
            && cells[0][2].getContent() == theSeed
            && cells[1][1].getContent() == theSeed
            && cells[2][0].getContent() == theSeed);
    }

    public void paint() {
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                cells[row][col].paint();
                if (col < COLS - 1) System.out.print("|");
            }
            System.out.println();
            if (row < ROWS - 1) {
                System.out.println("-----------");
            }
        }
    }
}
