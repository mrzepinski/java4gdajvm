package tictactoe;

import tictactoe.model.Board;
import tictactoe.model.Cell;
import tictactoe.model.GameState;
import tictactoe.model.Seed;

import java.util.Scanner;

public class TicTacToeManager {
    private Board board;
    private GameState currentState;
    private Seed currentPlayer;

    private static Scanner in = new Scanner(System.in);

    public void start() {
        board = new Board();
        initGame();
        do {
            playerMove(currentPlayer);
            board.paint();
            updateGame(currentPlayer);
            if (currentState == GameState.CROSS_WON) {
                System.out.println("'X' won! Bye!");
            } else if (currentState == GameState.NOUGHT_WON) {
                System.out.println("'O' won! Bye!");
            } else if (currentState == GameState.DRAW) {
                System.out.println("It's Draw! Bye!");
            }
            currentPlayer = (currentPlayer == Seed.CROSS) ? Seed.NOUGHT : Seed.CROSS;
        } while (currentState == GameState.PLAYING);
    }

    private void initGame() {
        board.init();
        currentPlayer = Seed.CROSS;
        currentState = GameState.PLAYING;
    }

    private void playerMove(Seed theSeed) {
        boolean validInput = false;
        do {
            if (theSeed == Seed.CROSS) {
                System.out.print("Player 'X', enter your move (row[1-3] column[1-3]): ");
            } else {
                System.out.print("Player 'O', enter your move (row[1-3] column[1-3]): ");
            }
            int row = in.nextInt() - 1;
            int col = in.nextInt() - 1;
            Cell[][] boardCells = board.getCells();
            if (row >= 0 && row < Board.getRows() && col >= 0 && col < Board.getCols() && boardCells[row][col].getContent() == Seed.EMPTY) {
                boardCells[row][col].setContent(theSeed);
                board.setCurrentRow(row);
                board.setCurrentCol(col);
                validInput = true;
            } else {
                System.out.println("This move at (" + (row + 1) + "," + (col + 1) + ") is not valid. Try again...");
            }
        } while (!validInput);
    }

    private void updateGame(Seed theSeed) {
        if (board.hasWon(theSeed)) {
            currentState = (theSeed == Seed.CROSS) ? GameState.CROSS_WON : GameState.NOUGHT_WON;
        } else if (board.isDraw()) {
            currentState = GameState.DRAW;
        }
    }
}
