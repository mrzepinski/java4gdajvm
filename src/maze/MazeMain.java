package maze;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MazeMain {
    public static void main(String[] args) throws IOException {
        int x = args.length >= 1 ? (Integer.parseInt(args[0])) : 32;
        int y = args.length == 2 ? (Integer.parseInt(args[1])) : 32;
        MazeSolver solver = new MazeSolver();
        if (args.length > 2) {
            InputStream f = new FileInputStream(args[0]);
            solver.readLines(f);
            solver.solveMaze();
        } else {
            MazeGenerator maze = new MazeGenerator(x, y);
            solver.solveMaze(maze.getMaze());
        }
        String[] solvedLines = solver.expandHorizontally();
        for (String solvedLine : solvedLines) {
            System.out.println(solvedLine);
        }
    }
}
